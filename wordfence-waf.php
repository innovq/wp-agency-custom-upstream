<?php
// Before removing this file, please verify the PHP ini setting `auto_prepend_file` does not point to this.

// This file was the current value of auto_prepend_file during the Wordfence WAF installation (Wed, 04 Jul 2018 15:51:47 +0000)
if (file_exists('/srv/bindings/188fa15d78b44e519bf32a11b3480a4e/includes/prepend.php')) {
	include_once '/srv/bindings/188fa15d78b44e519bf32a11b3480a4e/includes/prepend.php';
}
if (file_exists('/srv/bindings/188fa15d78b44e519bf32a11b3480a4e/code/wp-content/plugins/wordfence/waf/bootstrap.php')) {
	define("WFWAF_LOG_PATH", '/srv/bindings/188fa15d78b44e519bf32a11b3480a4e/code/wp-content/wflogs/');
	include_once '/srv/bindings/188fa15d78b44e519bf32a11b3480a4e/code/wp-content/plugins/wordfence/waf/bootstrap.php';
}
?>